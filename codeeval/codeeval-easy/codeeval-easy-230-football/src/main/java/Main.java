import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split(" \\| ");
			Map<Integer, List<Integer>> map = new HashMap<>();
			List<Integer> countries = new ArrayList<>();
			for (int i = 0; i < arr.length; i++) {
				String[] column = arr[i].split("\\s+");
				List<Integer> list = new ArrayList<>();
				for (String str : column) {
					Integer country = Integer.valueOf(str);
					if (!countries.contains(country)) {
						countries.add(country);
					}
					list.add(country);
				}
				map.put(i + 1, list);
			}
			Collections.sort(countries);
			StringBuilder output = new StringBuilder();
			for (int country : countries) {
				output.append(country);
				output.append(":");
				for (int column = 1; map.containsKey(column); column++) {
					if (map.get(column).contains(country)) {
						output.append(column);
						output.append(",");
					}
				}
				if (output.substring(output.length() - 1).compareTo(",") == 0) {
					output.setLength(output.length() - 1);
				}
				output.append("; ");
			}
			System.out.println(output);
		}
	}
}