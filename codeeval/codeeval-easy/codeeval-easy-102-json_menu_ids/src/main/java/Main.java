import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Main {
	public static void main(String[] args) throws IOException, ScriptException {
		ScriptEngineManager sem = new ScriptEngineManager();
		ScriptEngine engine = sem.getEngineByName("javascript");
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			if (!line.trim().isEmpty()) {
				String script = "Java.asJSONCompatible(" + line + ")";
				Object json = engine.eval(script);
				Map contents = (Map) json;
				List<Map> items = (List<Map>) ((Map) ((Map) contents).get("menu")).get("items");
				int sum = 0;
				for (Map map : items) {
					if (map != null && map.containsKey("label")) {
						sum += Integer.valueOf(map.get("id").toString());
					}
				}
				System.out.println(sum);
			}
		}
	}
}