import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split(";");
			List<Integer> distances = new ArrayList<>();
			for (int pos = 0; pos < arr.length; pos++) {
				distances.add(Integer.valueOf(arr[pos].split(",")[1]));
			}
			Collections.sort(distances);
			int starting = 0;
			int pos = 0;
			for (int distance : distances) {
				int gap = distance - starting;
				System.out.print(gap);
				if (pos < distances.size() - 1) {
					System.out.print(",");
				}
				starting = distance;
				pos++;
			}
			System.out.println();
		}
	}
}