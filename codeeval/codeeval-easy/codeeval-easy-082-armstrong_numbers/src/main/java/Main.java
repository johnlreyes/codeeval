import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			int number = Integer.valueOf(line);
			int sum = 0;
			for (int pos = 0; pos < line.length(); pos++) {
				int digit = Integer.valueOf(Character.toString(line.charAt(pos)));
				double powerOf = Math.pow((double) digit, (double) line.length());
				sum += powerOf;
			}
			System.out.println(sum == number ? "True" : "False");
		}
	}
}