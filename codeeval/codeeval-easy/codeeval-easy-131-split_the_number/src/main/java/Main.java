import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split("\\s+");
			String numbers = arr[0];
			String[] letters = arr[1].split("[\\-\\+]");
			int value1 = Integer.valueOf(numbers.substring(0, letters[0].length()));
			int value2 = Integer.valueOf(numbers.substring(letters[0].length()));
			if (line.contains("-")) {
				System.out.println(value1 - value2);
			} else {
				System.out.println(value1 + value2);
			}
		}
	}
}