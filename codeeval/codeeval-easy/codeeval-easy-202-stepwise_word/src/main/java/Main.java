import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split("\\s+");
			String longest = "";
			for (String word : arr) {
				if (word.length() > longest.length()) {
					longest = word;
				}
			}
			for (int i = 0; i < longest.length(); i++) {
				for (int h = 0; h < i; h++) {
					System.out.print("*");
				}
				System.out.print(longest.charAt(i));
				System.out.print(" ");
			}
			System.out.println();
		}
	}
}