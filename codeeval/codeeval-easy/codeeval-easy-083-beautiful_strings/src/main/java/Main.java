import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			Map<Character, Integer> charCounter = new LinkedHashMap<>();
			for (int i = 0; i < line.length(); i++) {
				if (Character.isLetter(line.charAt(i))) {
					char ch = Character.toLowerCase(line.charAt(i));
					int counter = 1;
					if (charCounter.containsKey(ch)) {
						counter = charCounter.get(ch) + 1;
					}
					charCounter.put(ch, counter);
				}
			}
			ArrayList<Integer> list = new ArrayList<>();
			list.addAll(charCounter.values());
			Collections.sort(list);
			Collections.reverse(list);
			int beauty = 26;
			int sum = 0;
			for (int occurence : list) {
				sum += beauty * occurence;
				beauty--;
			}
			System.out.println(sum);
		}
	}
}