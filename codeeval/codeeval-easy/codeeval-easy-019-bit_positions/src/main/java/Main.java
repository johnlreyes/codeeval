import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath(), Charset.defaultCharset());
		for (String line : lines) {
			String[] arr = line.split(",");
			int n = Integer.valueOf(arr[0]);
			int p1 = Integer.valueOf(arr[1]);
			int p2 = Integer.valueOf(arr[2]);
			String binaryString = Integer.toBinaryString(n);
			// System.out.println(n);
			// System.out.println(binaryString);
			int i1 = binaryString.length() - p1;
			int i2 = binaryString.length() - p2;
			// System.out.println("["+i1+"] "+binaryString.charAt(i1));
			// System.out.println("["+i2+"] "+binaryString.charAt(i2));
			System.out.println(binaryString.charAt(i1) == binaryString.charAt(i2));
		}
	}
}