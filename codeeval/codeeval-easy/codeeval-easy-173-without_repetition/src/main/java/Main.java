import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			StringBuilder output = new StringBuilder();
			char prev = ' ';
			for (int i=0; i<line.length(); i++) {
				if (prev != line.charAt(i)) {
					output.append(line.charAt(i));
				}
				prev = line.charAt(i);
			}
			System.out.println(output);
		}
	}
}