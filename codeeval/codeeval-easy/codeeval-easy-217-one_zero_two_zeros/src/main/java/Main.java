import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split("\\s+");
			int zero = Integer.valueOf(arr[0]);
			int matched = 0;
			int iteration = Integer.valueOf(arr[1]);
			for (int i = 1; i <= iteration; i++) {
				String bin = Integer.toBinaryString(i);
				int count = 0;
				for (int pos = 0; pos < bin.length(); pos++) {
					if (bin.charAt(pos) == '0') {
						count++;
					}
				}
				if (count == zero) {
					matched++;
				}
			}
			System.out.println(matched);
		}
	}
}