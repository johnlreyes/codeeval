import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split(" \\| ");
			List<Integer> numbers = new ArrayList<>();
			for (String item : arr[0].split("\\s+")) {
				numbers.add(Integer.valueOf(item));
			}
			int iteration = Integer.valueOf(arr[1]);
			for (int i = 0; i < iteration; i++) {
				for (int pos = 0; pos < numbers.size() - 1; pos++) {
					int current = numbers.get(pos);
					int next = numbers.get(pos + 1);
					if (current > next) {
						numbers.set(pos, next);
						numbers.set(pos + 1, current);
						break;
					}
				}
			}
			for (int number : numbers) {
				System.out.print(number);
				System.out.print(" ");
			}
			System.out.println();
		}
	}
}