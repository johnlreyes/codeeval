import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			double upper = 0;
			double lower = 0;
			for (int i = 0; i < line.length(); i++) {
				char ch = line.charAt(i);
				if (Character.isLetter(ch)) {
					if (Character.isUpperCase(ch)) {
						upper++;
					} else {
						lower++;
					}
				}
			}
			System.out.printf("lowercase: %.2f uppercase: %.2f \n", ((lower / (lower + upper)) * 100),
					((upper / (upper + lower))) * 100);
		}
	}
}