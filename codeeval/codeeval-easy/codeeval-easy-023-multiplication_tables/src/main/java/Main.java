import java.io.IOException;

public class Main {
	public static void main(String... args) throws IOException {
		for (int i = 1; i <= 12; i++) {
			for (int h = 1; h <= 12; h++) {
				System.out.printf("%4d", i * h);
			}
			if (i < 12) {
				System.out.println();
			}
		}
	}
}