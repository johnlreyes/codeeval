import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Main {
	private static final List<String> NUMBER_0 = new ArrayList<>();
	static {
		NUMBER_0.add("-**-");
		NUMBER_0.add("*--*");
		NUMBER_0.add("*--*");
		NUMBER_0.add("*--*");
		NUMBER_0.add("-**-");
	}
	private static final List<String> NUMBER_1 = new ArrayList<>();
	static {
		NUMBER_1.add("--*-");
		NUMBER_1.add("-**-");
		NUMBER_1.add("--*-");
		NUMBER_1.add("--*-");
		NUMBER_1.add("-***");
	}
	private static final List<String> NUMBER_2 = new ArrayList<>();
	static {
		NUMBER_2.add("***-");
		NUMBER_2.add("---*");
		NUMBER_2.add("-**-");
		NUMBER_2.add("*---");
		NUMBER_2.add("****");
	}
	private static final List<String> NUMBER_3 = new ArrayList<>();
	static {
		NUMBER_3.add("***-");
		NUMBER_3.add("---*");
		NUMBER_3.add("-**-");
		NUMBER_3.add("---*");
		NUMBER_3.add("***-");
	}
	private static final List<String> NUMBER_4 = new ArrayList<>();
	static {
		NUMBER_4.add("-*--");
		NUMBER_4.add("*--*");
		NUMBER_4.add("****");
		NUMBER_4.add("---*");
		NUMBER_4.add("---*");
	}
	private static final List<String> NUMBER_5 = new ArrayList<>();
	static {
		NUMBER_5.add("****");
		NUMBER_5.add("*---");
		NUMBER_5.add("***-");
		NUMBER_5.add("---*");
		NUMBER_5.add("***-");
	}
	private static final List<String> NUMBER_6 = new ArrayList<>();
	static {
		NUMBER_6.add("-**-");
		NUMBER_6.add("*---");
		NUMBER_6.add("***-");
		NUMBER_6.add("*--*");
		NUMBER_6.add("-**-");
	}
	private static final List<String> NUMBER_7 = new ArrayList<>();
	static {
		NUMBER_7.add("****");
		NUMBER_7.add("---*");
		NUMBER_7.add("--*-");
		NUMBER_7.add("-*--");
		NUMBER_7.add("-*--");
	}
	private static final List<String> NUMBER_8 = new ArrayList<>();
	static {
		NUMBER_8.add("-**-");
		NUMBER_8.add("*--*");
		NUMBER_8.add("-**-");
		NUMBER_8.add("*--*");
		NUMBER_8.add("-**-");
	}
	private static final List<String> NUMBER_9 = new ArrayList<>();
	static {
		NUMBER_9.add("-**-");
		NUMBER_9.add("*--*");
		NUMBER_9.add("-***");
		NUMBER_9.add("---*");
		NUMBER_9.add("-**-");
	}
	private static final List<List<String>> NUMBERS = new ArrayList<>();
	static {
		NUMBERS.add(NUMBER_0);
		NUMBERS.add(NUMBER_1);
		NUMBERS.add(NUMBER_2);
		NUMBERS.add(NUMBER_3);
		NUMBERS.add(NUMBER_4);
		NUMBERS.add(NUMBER_5);
		NUMBERS.add(NUMBER_6);
		NUMBERS.add(NUMBER_7);
		NUMBERS.add(NUMBER_8);
		NUMBERS.add(NUMBER_9);
		for (List<String> number : NUMBERS) {
			number.add("----");
		}
	}

	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String digits = line.replaceAll("\\D*", "");
			for (int row = 0; row < 6; row++) {
				for (int i = 0; i < digits.length(); i++) {
					int number = Integer.valueOf(Character.toString(digits.charAt(i)));
					System.out.print(NUMBERS.get(number).get(row));
					System.out.print("-");
				}
				System.out.println();
			}
		}
	}
}