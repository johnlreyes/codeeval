import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.substring(1, line.length() - 1).split("\\) \\(");
			String[] c1 = arr[0].split(",");
			int x1 = Integer.valueOf(c1[0].trim());
			int y1 = Integer.valueOf(c1[1].trim());
			String[] c2 = arr[1].split(",");
			int x2 = Integer.valueOf(c2[0].trim());
			int y2 = Integer.valueOf(c2[1].trim());
			System.out.println((int)Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
		}
	}
}