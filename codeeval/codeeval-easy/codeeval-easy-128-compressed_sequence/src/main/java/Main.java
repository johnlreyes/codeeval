import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			List<Integer> list = Arrays.asList(line.split("\\s+")).stream().map(e -> Integer.parseInt(e))
					.collect(Collectors.toList());
			int prev = -1;
			int count = 1;
			for (int current : list) {
				if (prev != -1) {
					if (prev == current) {
						count++;
					} else {
						System.out.print(count + " " + prev + " ");
						count = 1;
					}
				}
				prev = current;
			}
			System.out.print(count + " " + prev + " ");
			System.out.println();
		}
	}
}
