import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
	private static Map<String, String> morseCodes = createMorseCode();

	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] words = line.split("  ");
			for (String word : words) {
				String[] chars = word.split(" ");
				for (String ch : chars) {
					String s = morseCodes.get(ch);
					System.out.print(s == null ? " " : s);
				}
				System.out.print(" ");
			}
			System.out.println();
		}
		System.out.println();
	}

	private static Map<String, String> createMorseCode() {
		Map<String, String> returnValue = new HashMap<>();
		returnValue.put(".-", "A");
		returnValue.put("-...", "B");
		returnValue.put("-.-.", "C");
		returnValue.put("-..", "D");
		returnValue.put(".", "E");
		returnValue.put("..-.", "F");
		returnValue.put("--.", "G");
		returnValue.put("....", "H");
		returnValue.put("..", "I");
		returnValue.put(".---", "J");
		returnValue.put("-.-", "K");
		returnValue.put(".-..", "L");
		returnValue.put("--", "M");
		returnValue.put("-.", "N");
		returnValue.put("---", "O");
		returnValue.put(".--.", "P");
		returnValue.put("--.-", "Q");
		returnValue.put(".-.", "R");
		returnValue.put("...", "S");
		returnValue.put("-", "T");
		returnValue.put("..-", "U");
		returnValue.put("...-", "V");
		returnValue.put(".--", "W");
		returnValue.put("-..-", "X");
		returnValue.put("-.--", "Y");
		returnValue.put("--..", "Z");
		returnValue.put("-----", "0");
		returnValue.put(".----", "1");
		returnValue.put("..---", "2");
		returnValue.put("...--", "3");
		returnValue.put("....-", "4");
		returnValue.put(".....", "5");
		returnValue.put("-....", "6");
		returnValue.put("--...", "7");
		returnValue.put("---..", "8");
		returnValue.put("----.", "9");
		return returnValue;
	}
}