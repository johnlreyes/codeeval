import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			System.out.println(isHappy(Integer.valueOf(line))? 1: 0);
		}
	}

	private static boolean isHappy(int n) {
		List<Integer> seen = new ArrayList<Integer>();
		while (!seen.contains(n)) {
			seen.add(n);
			int sum = 0;
			while (n > 0) {
				int last = n % 10;
				sum += last * last;
				n /= 10;
			}
			n = sum;
		}
		return n==1;
	}
}