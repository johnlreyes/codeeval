import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath(), Charset.defaultCharset());
		for (String line : lines) {
			StringBuilder output = new StringBuilder();
			String[] numberArr = line.split("\\s+");
			int limit = Integer.valueOf(numberArr[2]);
			int fizz = Integer.valueOf(numberArr[0]);
			int buzz = Integer.valueOf(numberArr[1]);
			for (int i = 1; i <= limit; i++) {
				StringBuilder fizzBuzz = new StringBuilder();
				if (i % fizz == 0) {
					fizzBuzz.append("F");
				}
				if (i % buzz == 0) {
					fizzBuzz.append("B");
				}
				if (fizzBuzz.length() == 0) {
					fizzBuzz.append(i);
				}
				output.append(fizzBuzz);
				output.append(" ");
			}
			System.out.println(output.delete(output.length() - 1, output.length()));
		}
	}
}