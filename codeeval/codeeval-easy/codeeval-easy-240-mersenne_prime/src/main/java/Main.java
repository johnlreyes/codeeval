import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			StringBuilder output = new StringBuilder();
			for (int i = 0; i <= Integer.valueOf(line); i++) {
				if (isPrime(i) && isMersenne(i)) {
					output.append(i + ", ");
				}
			}
			if (output.length() > 0) {
				output.setLength(output.length() - 2);
			}
			System.out.println(output);
		}
	}

	static boolean isPrime(int n) {
		for (int i = 2; i < n; i++) {
			if (n % i == 0)
				return false;
		}
		return true;
	}

	static boolean isMersenne(int n) {
		if (n > 2) {
			int powerOf2 = 0;
			for (int i = 0;; i++) {
				powerOf2 = (int) Math.pow(2, i);
				if (powerOf2 > (n + 1)) {
					break;
				} else if (powerOf2 == (n + 1)) {
					return true;
				}
			}
		}
		return false;
	}
}