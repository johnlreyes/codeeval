import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			List<Integer> highest = new ArrayList<Integer>();
			String[] allScores = line.split(" \\| ");
			for (String scoresPerStudent : allScores) {
				String[] scoresPerStudentArr = scoresPerStudent.split(" ");
				int index = 0;
				for (String scoreStr : scoresPerStudentArr) {
					if (highest.isEmpty()) {
						for (int i = 0; i < scoresPerStudentArr.length; i++) {
							highest.add(0);
						}
					}
					int score = Integer.valueOf(scoreStr);
					if (score > highest.get(index)) {
						highest.set(index, score);
					}
					index++;
				}
			}
			for (Integer n : highest) {
				System.out.printf("%d ", n);
			}
			System.out.println();
		}
	}
}