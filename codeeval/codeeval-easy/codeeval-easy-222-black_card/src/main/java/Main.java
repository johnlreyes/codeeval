import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split(" \\| ");
			List<String> players = new ArrayList<>();
			players.addAll(Arrays.asList(arr[0].split("\\s+")));
			int number = Integer.valueOf(arr[1]);
			while (players.size() > 1) {
				int removeIndex = players.size() - 1;
				if (number > players.size() && number % players.size() != 0) {
					removeIndex = number - ((number / players.size()) * players.size());
					if (removeIndex > 0) {
						removeIndex--;
					}
				}
				players.remove(removeIndex);
			}
			System.out.println(players.get(0));
		}
	}
}