import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			int self = 1;
			for (int pos = 0; pos < line.length() && self == 1; pos++) {
				int digit = Integer.valueOf(Character.toString(line.charAt(pos)));
				int count = 0;
				for (int i = 0; i < line.length(); i++) {
					if (Integer.valueOf(Character.toString(line.charAt(i))) == pos) {
						count++;
					}
				}
				self = count == digit ? 1 : 0;
			}
			System.out.println(self);
		}
	}
}