import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split(",");
			int L = arr.length / 2;
			Set<String> set = Arrays.stream(arr)
					.collect(Collectors.groupingBy(Function.identity(), Collectors.counting())).entrySet().stream()
					.filter(map -> map.getValue() > L).collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()))
					.keySet();
			System.out.println(set.isEmpty() ? "None" : set.iterator().next());
		}
	}
}
