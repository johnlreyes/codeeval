import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split(";");
			List<String> words = Arrays.asList(arr[0].split("\\s+"));
			List<Integer> indexes = new ArrayList<>();
			String[] output = new String[words.size()];
			for (String number : arr[1].split("\\s+")) {
				indexes.add(Integer.valueOf(number));
			}
			for (int i = 1; i <= words.size(); i++) {
				if (!indexes.contains(i)) {
					indexes.add(i);
					break;
				}
			}
			for (int pos = 0; pos < indexes.size(); pos++) {
				int index = indexes.get(pos);
				output[index - 1] = words.get(pos);
			}
			for (String s : output) {
				System.out.print(s);
				System.out.print(" ");
			}
			System.out.println();
		}
	}
}