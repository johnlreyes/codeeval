import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split("\\s+");
			boolean zero = false;
			StringBuilder binary = new StringBuilder();
			for (int i = 0; i < arr.length; i++) {
				if (i % 2 == 0) {
					zero = arr[i].length() == 1;
				} else {
					for (int h = 0; h < arr[i].length(); h++) {
						binary.append(zero ? 0 : 1);
					}
				}
			}
			System.out.println(Long.parseLong(binary.toString(), 2));
		}
	}
}