import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split(" \\| ");
			String[] cards = arr[0].split("\\s+");
			char trumpSuit = arr[1].charAt(0);
			char card1Suit = cards[0].charAt(cards.length - 1);
			char card2Suit = cards[1].charAt(cards.length - 1);
			if (card1Suit == trumpSuit && card2Suit != trumpSuit) {
				System.out.println(cards[0]);
			} else if (card2Suit == trumpSuit && card1Suit != trumpSuit) {
				System.out.println(cards[1]);
			} else {
				int n1 = cardNumber(cards[0].substring(0, cards[0].length() - 1));
				int n2 = cardNumber(cards[1].substring(0, cards[1].length() - 1));
				if (n1 == n2) {
					System.out.println(cards[0] + " " + cards[1]);
				} else if (n1 > n2) {
					System.out.println(cards[0]);
				} else {
					System.out.println(cards[1]);
				}
			}
		}
	}

	static int cardNumber(String card) {
		int returnValue = -1;
		if (card.compareTo("J") == 0) {
			returnValue = 11;
		} else if (card.compareTo("Q") == 0) {
			returnValue = 12;
		} else if (card.compareTo("K") == 0) {
			returnValue = 13;
		} else if (card.compareTo("A") == 0) {
			returnValue = 14;
		} else {
			returnValue = Integer.valueOf(card);
		}
		return returnValue;

	}
}