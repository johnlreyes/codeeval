import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			// System.out.println(line);
			String[] arr = line.split(" \\| ");
			String[] wines = arr[0].split(" ");
			String letters = arr[1];
			String output = "False";
			for (String wine : wines) {
				Map<Character, Boolean> map = new HashMap<>();
				for (int pos = 0; pos < letters.length(); pos++) {
					char ch = letters.charAt(pos);
					map.put(ch, wine.contains("" + ch));
				}
				boolean hasLetters = true;
				for (Map.Entry<Character, Boolean> item : map.entrySet()) {
					if (!item.getValue()) {
						hasLetters = false;
						break;
					}
				}
				if (hasLetters) {
					output = wine;
					break;
				}
			}
			System.out.println(output);
		}
	}
}