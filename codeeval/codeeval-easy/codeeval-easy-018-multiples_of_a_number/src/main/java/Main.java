import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath(), Charset.defaultCharset());
		for (String line : lines) {
			String[] numberArr = line.split(",");
			int x = Integer.valueOf(numberArr[0]);
			int n = Integer.valueOf(numberArr[1]);
			int output = 0;
			while (output < x) {
				output += n;
			}
			System.out.println(output);
		}
	}
}