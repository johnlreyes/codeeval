import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split(" \\| ");
			String developer = arr[0];
			String design = arr[1];
			int bugs = 0;
			for (int i = 0; i < design.length(); i++) {
				if (design.charAt(i) != developer.charAt(i)) {
					bugs++;
				}
			}
			String status = "Critical";
			switch (bugs) {
			case 0:
				status = "Done";
				break;
			case 1:
			case 2:
				status = "Low";
				break;
			case 3:
			case 4:
				status = "Medium";
				break;
			case 5:
			case 6:
				status = "High";
				break;
			}
			System.out.println(status);
		}
	}
}