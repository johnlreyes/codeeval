import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			double number = Double.parseDouble(line);
			double fractionMins = number - ((int) number);
			int minutes = (int) (fractionMins * 60);
			double fractionSecs = (fractionMins * 60) - minutes;
			int seconds = (int) (fractionSecs * 60);
			System.out.printf("%d.%02d'%02d\"\n", (int) number, minutes, seconds);
		}
	}
}