import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			List<Integer> numbers = new ArrayList<>();
			List<String> words = new ArrayList<>();
			String[] arr = line.split(",");
			for (String str : arr) {
				try {
					numbers.add(Integer.parseInt(str));
				} catch (NumberFormatException ex) {
					words.add(str);
				}
			}
			for (int i = 0; i < words.size(); i++) {
				System.out.print(words.get(i));
				if (i < words.size() - 1) {
					System.out.print(",");
				}
			}
			if (!words.isEmpty() && !numbers.isEmpty()) {
				System.out.print("|");
			}
			for (int i = 0; i < numbers.size(); i++) {
				System.out.print(numbers.get(i));
				if (i < numbers.size() - 1) {
					System.out.print(",");
				}
			}
			System.out.println();
		}
	}
}
