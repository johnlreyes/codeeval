import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split(" \\| ");
			String[] left = arr[0].split(" ");
			String[] right = arr[1].split(" ");
			for (int i = 0; i < left.length; i++) {
				System.out.print(Integer.valueOf(left[i]) * Integer.valueOf(right[i]));
				System.out.print(" ");
			}
			System.out.println();
		}
	}
}