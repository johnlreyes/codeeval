import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] chars = line.split(" ");
			int N = 0;
			for (int i = 10; i >= 1; i--) {
				if (i * i == chars.length) {
					N = i;
					break;
				}
			}
			char[][] matrix = new char[N][N];
			for (int i = 0; i < chars.length;) {
				for (int x = 0; x < N; x++) {
					for (int y = 0; y < N; y++) {
						matrix[x][y] = chars[i].charAt(0);
						i++;
					}
				}
			}
			rotate(matrix);
			for (int x = 0; x < N; x++) {
				for (int y = 0; y < N; y++) {
					System.out.print(matrix[x][y]);
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}

	static void rotate(char[][] matrix) {
		int N = matrix.length;
		for (int layer = 0; layer < N / 2; layer++) {
			for (int element = layer; element < N - layer - 1; element++) {
				char current = matrix[layer][element];
				matrix[layer][element] = matrix[N - element - 1][layer];
				matrix[N - element - 1][layer] = matrix[N - layer - 1][N - element - 1];
				matrix[N - layer - 1][N - element - 1] = matrix[element][N - layer - 1];
				matrix[element][N - layer - 1] = current;
			}
		}
	}
}