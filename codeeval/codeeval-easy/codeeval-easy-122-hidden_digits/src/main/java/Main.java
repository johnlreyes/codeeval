import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			StringBuilder output = new StringBuilder();
			for (int pos = 0; pos < line.length(); pos++) {
				char ch = line.charAt(pos);
				if (Character.isDigit(ch)) {
					output.append(ch);
				} else if (ch >= 'a' && ch <= 'j') {
					output.append(ch - 'a');
				}
			}
			if (output.length() == 0) {
				output.append("NONE");
			}
			System.out.println(output);
		}
	}
}
