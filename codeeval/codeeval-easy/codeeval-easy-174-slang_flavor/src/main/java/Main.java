import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Main {
	private static List<String> slangs = new ArrayList<>();
	static {
		slangs.add(", yeah!");
		slangs.add(", this is crazy, I tell ya.");
		slangs.add(", can U believe this?");
		slangs.add(", eh?");
		slangs.add(", aw yea.");
		slangs.add(", yo.");
		slangs.add("? No way!");
		slangs.add(". Awesome!");
	}

	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		int count = 0;
		int slangIndex = 0;
		for (String line : lines) {
			String[] arr = line.split("[\\.!\\?]");
			for (String item : arr) {
				if (count % 2 != 0) {
					System.out.print(item);
					System.out.print(slangs.get(slangIndex));
					slangIndex = (slangIndex + 1) % slangs.size();
				} else {
					System.out.print(item);
					int startOfOrigPunch = line.indexOf(item)+item.length();
					System.out.print(line.substring(startOfOrigPunch, startOfOrigPunch+1));
				}
				count++;
			}
			System.out.println();
		}
	}
}