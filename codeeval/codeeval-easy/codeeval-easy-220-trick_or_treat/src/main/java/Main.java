import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split(", ");
			int vampires = Integer.valueOf(arr[0].split(": ")[1]);
			int zombies = Integer.valueOf(arr[1].split(": ")[1]);
			int witches = Integer.valueOf(arr[2].split(": ")[1]);
			int houses = Integer.valueOf(arr[3].split(": ")[1]);
			System.out.println(((3 * vampires + 4 * zombies + 5 * witches) * houses) / (vampires + zombies + witches));
		}
	}
}