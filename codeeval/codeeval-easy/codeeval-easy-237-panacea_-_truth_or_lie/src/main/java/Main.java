import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(new File(args[0]).toPath());
		for (String line : lines) {
			String[] arr = line.split(" \\| ");
			int hexSum = 0;
			for (String str : arr[0].split(" ")) {
				hexSum += Integer.valueOf(str, 16);
			}
			int binSum = 0;
			for (String str : arr[1].split(" ")) {
				binSum += Integer.valueOf(str, 2);
			}
			System.out.println(binSum >= hexSum ? "True" : "False");
		}
	}
}